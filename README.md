# Tanks

This is a tiny game made to learn the Godot game engine, version 3.1.

See also [the itch.io page](https://remram44.itch.io/tanks).

![screenshot](screenshot.png)

## I want to help

Contributions to the art are welcome. I'm a decent programmer, but an awful artist. Feedback is also appreciated.
