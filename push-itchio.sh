#!/bin/sh
set -eu

if [ "x$1" = x ]; then
    echo "Version?" >&2
    exit 1
fi

GAME="remram44/tanks"

butler push windows ${GAME}:win32 --userversion $1
butler push linux ${GAME}:linux-amd64 --userversion $1
butler push macos/Tanks.zip ${GAME}:macos --userversion $1
