#!/bin/sh
set -eu

if [ "x$1" = x ]; then
    echo "Version?" >&2
    exit 1
fi
WORKDIR="$(pwd)"

rm_except(){
    for i in *; do
        [ "$i" = "$1" ] || rm "$i"
    done
}

cd "$WORKDIR/linux"
tar zcf tanks-$1-linux-amd64.tar.gz *
rm_except tanks-$1-linux-amd64.tar.gz

cd "$WORKDIR/windows"
zip tanks-$1-windows.zip *
rm_except tanks-$1-windows.zip

cd "$WORKDIR/macos"
mv Tanks.zip tanks-$1-macos.zip
