extends Node

static func angle_wrap(angle):
    return fmod(angle + 9.0 * PI, 2.0 * PI) - PI
