extends Node

signal player_joined

const ACTIONS = ["left", "right", "up", "down",
                 "turn_clockwise", "turn_counter", "shoot"]

var who_moving = {}

func _ready():
    pause_mode = Node.PAUSE_MODE_PROCESS

func _process(delta):
    # FIXME: This is ugly and inefficient
    # replace with a dictionary?
    # See also https://github.com/godotengine/godot/issues/15681
    for player in ["key1", "key2", "joy1", "joy2"]:
        for action in ACTIONS:
            if Input.is_action_pressed("%s_%s" % [player, action]):
                if not who_moving.has(player):
                    print("player %s joined" % player)
                    who_moving[player] = true
                    emit_signal("player_joined", player, who_moving)

func reset():
    """Reset the list of players before a new round.
    """
    who_moving = {}
