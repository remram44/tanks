extends Spatial

const MAX_PLAYERS = 4
const MAPS = ["Map1", "Map2"]

var tank = preload("res://actors/Tank.tscn")

onready var inputs = $"/root/inputs"

var countdown = 3
var players_alive = 0
var map = null
var map_index = len(MAPS)
var tanks = null
var random = RandomNumberGenerator.new()

func _ready():
    add_child(tanks)
    random.randomize()
    inputs.connect("player_joined", self, "player_joined")
    new_round()

func new_round():
    # Remove all tanks
    if tanks:
        tanks.queue_free()
    tanks = Spatial.new()
    add_child(tanks)
    players_alive = 0

    # Pick a map, avoiding the one we just played
    var new_map_index
    if map:
        new_map_index = random.randi_range(0, len(MAPS) - 2)
    else:
        new_map_index = random.randi_range(0, len(MAPS) - 1)
    if new_map_index >= map_index:
        map_index = new_map_index + 1
    else:
        map_index = new_map_index
    # Load it
    if map:
        map.queue_free()
    map = load("res://maps/" + MAPS[map_index] + ".tscn").instance()
    add_child(map)

    # Start spawning players
    inputs.reset()
    print("Found %d spawn points" % len(get_tree().get_nodes_in_group("spawn_points")))

func player_joined(new_player, all_players):
    # Find a spawn point for the new player
    var spawn_points = get_tree().get_nodes_in_group("spawn_points")
    if not spawn_points:
        print("No more spawn points for player %s" % new_player)
        return
    var spawn_point = spawn_points[0]

    # Spawn a tank
    var new_tank = tank.instance()
    new_tank.global_transform = spawn_point.global_transform
    new_tank.player_id = new_player
    new_tank.connect("died", self, "player_lost")
    players_alive += 1
    tanks.add_child(new_tank)

    # Delete the spawn point itself
    spawn_point.remove_from_group("spawn_points")
    spawn_point.queue_free()

    # If we have 2 players, commence countdown
    if len(all_players) < 2:
        get_tree().paused = true
    elif len(all_players) == 2:
        $StartCountDown.start()
        $Clock.visible = true
        countdown = 3
        $Clock.text = "%s..." % countdown

func countdown():
    countdown -= 1
    $Clock.text = "%s..." % countdown
    if countdown == 0:
        $StartCountDown.stop()
        # Start the game!
        get_tree().paused = false
        $Clock.visible = false

        # Remove the rest of the spawn points, since they're visible
        for spawn_point in get_tree().get_nodes_in_group("spawn_points"):
            spawn_point.queue_free()

func player_lost():
    players_alive -= 1
    print("%s players left" % players_alive)
    if players_alive <= 1:
        # Start timer to new round
        $ResetCountDown.start()
